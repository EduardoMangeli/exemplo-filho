import { Externo } from "./Style"
import {FaBeer, FaFootballBall} from 'react-icons/fa'

export const Footer = () => (
  <Externo>
    <span><FaBeer/></span>
    <span><FaFootballBall/></span>
  </Externo>
)