import styled from "styled-components";
import { desktop, telaGrande } from "../../utils";

const Externo = styled.footer`
  background-color: aliceblue;
  display: flex;
  flex-direction: row;
  padding-right: 2em;
  justify-content: center;
  
  span {
    margin: 0 5px;
    font-size: 7em;
  }

  @media screen and (min-width: ${desktop}){
    justify-content: flex-end;

    span {
      font-size: 2em;
    }
  }


  @media screen and (min-width: ${telaGrande}){
    span {
      font-size: 3em;
    }
  }
`;

export { Externo };