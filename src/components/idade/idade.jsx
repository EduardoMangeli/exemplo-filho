import {useState} from 'react';

const Idade = () => {
    const [idade, setIdade] = useState(20);

    const mudaIdade = () => {
        setIdade(40);
    }

    return (
        <div>
            <p>Sua idade é {idade}.</p>

            <button onClick={mudaIdade}>Mudar Idade</button>
            <br/>
            <input value={idade} onChange={(e) => setIdade(e.target.value)}/>
        </div>
    );
}

export default Idade;