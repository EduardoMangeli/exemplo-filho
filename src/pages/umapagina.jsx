import Contador from "../components/contador/contador"
import Garagem from "../components/garagem/garagem"
import Idade from "../components/idade/idade"
import { Masterpage } from "./masterpage"

export const UmaPagina = () => (
  <Masterpage>
    <Contador/>
    <br/>
    <Idade/>
    <br/>
    <Garagem/>
  </Masterpage>
)