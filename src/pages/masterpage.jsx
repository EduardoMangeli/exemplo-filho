import { Footer } from "../components/footer/footer";
import { Header } from "../components/header/header";

export const Masterpage = (props) => (
  <>
    <Header/>
    <div className='masterContainer'>
    {props.children}
    </div>
    <Footer/>
  </>
)